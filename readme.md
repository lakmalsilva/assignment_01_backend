Assignment 01 (backend)
=======================

This project provide backend functionality for Assignment 01.

Deployment
==========
1. Clone the project
2. Install dependencies: "npm install"
3. Start server: "npm start"